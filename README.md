<<<<<<< README.md
## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Wed Apr 26 2023 17:13:56 GMT+0000 (GMT)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>1.9.0|
|**Generation Platform**<br>CLI|
|**Template Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.104.12.91:8000//sap/opu/odata/sap/ZODATA_INWI_SD_SRV
|**Module Name**<br>pfeproduct|
|**Application Title**<br>PFE application for product|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.113.0|
|**Enable Code Assist Libraries**<br>False|
|**Enable TypeScript**<br>False|
|**Add Eslint configuration**<br>False|
|**Object collection**<br>ZPRODUCTSet|
|**Object collection key**<br>Productid|
|**Object ID**<br>Productid|
|**Object number**<br>Price|
|**Object unit of measure**<br>Currencycode|

## pfeproduct

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

- It is also possible to run the application using mock data that reflects the OData Service URL supplied during application generation.  In order to run the application with Mock Data, run the following from the generated app root folder:

```
    npm run start-mock
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


=======
# INWI_PFE