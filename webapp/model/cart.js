sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
], function (
	MessageBox,
	MessageToast,
	) {
	"use strict";

	return {

		/**
		 * Checks for the status of the product that is added to the cart.
		 * If the product is not available, a message dialog will open.
		 * @public
		 * @param {Object} oBundle i18n bundle
		 * @param {Object} oProduct Product that is added to the cart
		 * @param {Object} oCartModel Cart model
		 */
		addToCart: function (oBundle, oProduct, oCartModel, oCartLengthModel) {
			// Items to be added from the welcome view have it's content inside product object
			if (oProduct.Product !== undefined) {
				oProduct = oProduct.Product;
			}
			switch (oProduct.Status) {
				case "D":
					//show message dialog
					MessageBox.show(
						oBundle.getText("productStatusDiscontinuedMsg"), {
							icon: MessageBox.Icon.ERROR,
							titles: oBundle.getText("productStatusDiscontinuedTitle"),
							actions: [MessageBox.Action.CLOSE]
						});
					break;
				case "O":
					// show message dialog
					MessageBox.show(
						oBundle.getText("productStatusOutOfStockMsg"), {
							icon: MessageBox.Icon.QUESTION,
							title: oBundle.getText("productStatusOutOfStockTitle"),
							actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
							onClose: function (oAction) {
								// order
								if (MessageBox.Action.OK === oAction) {
									this._updateCartItem(oBundle, oProduct, oCartModel);
								}
							}.bind(this)
						});
					break;
				case "A":
				default:
					this._updateCartItem(oBundle, oProduct, oCartModel, oCartLengthModel);
					break;
			}
		},

		/**
		 * Function that updates the cart model when a product is added to the cart.
		 * If the product is already in the cart the quantity is increased.
		 * If not, the product is added to the cart with quantity 1.
		 * @private
		 * @param {Object} oBundle i18n bundle
		 * @param {Object} oProductToBeAdded Product that is added to the cart
		 * @param {Object} oCartModel Cart model
		 */
		_updateCartItem: function (oBundle, oProductToBeAdded, oCartModel, oCartLengthModel) {
			// find existing entry for product
			var oCollectionEntries = Object.assign({}, oCartModel.getData()["cartEntries"]);
			var oCartEntry =  oCollectionEntries[oProductToBeAdded.Productid];
			console.log(oCartModel)
			oProductToBeAdded["ProductPrice"] = "";
			if (oCartEntry === undefined) {
				// create new entry
				oProductToBeAdded["ProductQuantity"] = Number(oProductToBeAdded["Quantity"]);
				oCartEntry = Object.assign({}, oProductToBeAdded);
				console.log(oCartEntry.ProductQuantity);
				oCartEntry.Quantity = 1;
				oCartEntry.ProductPrice = oCartEntry.Price;
				oCollectionEntries[oProductToBeAdded.Productid] = oCartEntry;
			} else {
				// update existing entry
				oCartEntry.Quantity += 1;
				oCartEntry.ProductPrice = oCartEntry.Quantity * oCartEntry.Price;
			}
			this.updateCartLength(oCollectionEntries, oCartLengthModel);
			//update the cart model
			oCartModel.setProperty("/cartEntries", Object.assign({}, oCollectionEntries));
			oCartModel.refresh(true);
			MessageToast.show(oBundle.getText("Your product added successfully" ));
		},

		updatePrice: function(productId, oCartModel, oCartLengthModel){
			var oCollectionEntries = Object.assign({}, oCartModel.getData()["cartEntries"]);
			var oCartEntry = oCollectionEntries[productId];
			oCartEntry.ProductPrice = oCartEntry.Price * oCartEntry.Quantity
			if(oCartEntry.Quantity == 0){
				this.deleteFromCart(productId, oCartModel);
			}
			this.updateCartLength(oCollectionEntries, oCartLengthModel);
		},
		
		deleteFromCart: function(productId, oCartModel, oCartLengthModel){

			var oCollectionEntries = Object.assign({}, oCartModel.getData()["cartEntries"]);
			delete  oCollectionEntries[productId];

			
			oCartModel.setProperty("/cartEntries", Object.assign({}, oCollectionEntries));
			oCartModel.refresh(true);
			this.updateCartLength(oCollectionEntries, oCartLengthModel);
		},

		updateCartLength: function(oCollectionEntries, oCartLengthModel){
			var length = Object.values(oCollectionEntries).length;
			//var oCartModel = this.getView("pfeproduct.view.App").getModel("cartLength");
			var totalPrice = 0;

			let keys = Object.keys(oCollectionEntries)
			for(const item of keys){
				var product = oCollectionEntries[item];
				if(product){
					totalPrice += Number(product.ProductPrice);
				}
			}

			oCartLengthModel.setProperty("/cartLength", length);
			oCartLengthModel.setProperty("/totalPrice", totalPrice);
		}
	};
});