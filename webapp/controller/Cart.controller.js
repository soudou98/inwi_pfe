sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
	"../model/formatter",
    "sap/m/MessageToast",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "../model/cart",
], function (BaseController, JSONModel,formatter, MessageToast, Filter, FilterOperator, cart) {
    "use strict";

    return BaseController.extend("pfeproduct.controller.Cart", {
		formatter: formatter,
        cart: cart,


        onInit : function () {
            // apply content density mode to root view
            this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
        },

        onCheckout: function() {
            var oCart = this.getModel("cartProducts");
            var oModel = this.getModel();
            var oCartList = Object.values(oCart.getData()["cartEntries"])

            if(oCartList.length > 0){
                this.getRouter().navTo("checkout", {});
            }else{
                 var msg = 'Please fill your cart with products';
                 MessageToast.show(msg);
             }
        },



        changePriceValue: function(oEvent){
            var productId = oEvent.getSource().getParent().getCells()[5].mProperties.text;
            var oCartModel = this.getView().getModel("cartProducts");
            var oCartLengthModel = this.getView("pfeproduct.view.App").getModel("cartLength");

            cart.updatePrice(productId, oCartModel, oCartLengthModel);
        },

    });

});