
sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/List",
    "sap/m/StandardListItem",
    "sap/m/Text",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
], function (BaseController, JSONModel, formatter, Dialog, Button, List, StandardListItem, Text, Filter, FilterOperator) {


    return BaseController.extend("pfeproduct.controller.Worklist", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        /**
         * Called when the worklist controller is instantiated.
         * @public
         */
        onInit : function () {

            var oViewModel;

            // keeps the search state
            this._aTableSearchState = [];

            // Model used to manipulate control states
            oViewModel = new JSONModel({
                worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
                shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
                shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
                tableNoDataText : this.getResourceBundle().getText("tableNoDataText")
            });
            this.setModel(oViewModel, "worklistView");
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Triggered by the table's 'updateFinished' event: after new table
         * data is available, this handler method updates the table counter.
         * This should only happen if the update was successful, which is
         * why this handler is attached to 'updateFinished' and not to the
         * table's list binding's 'dataReceived' method.
         * @param {sap.ui.base.Event} oEvent the update finished event
         * @public
         */
        onUpdateFinished : function (oEvent) {
            // update the worklist's object counter after the table update
            var sTitle,
                oTable = oEvent.getSource(),
                iTotalItems = oEvent.getParameter("total");
            // only update the counter if the length is final and
            // the table is not empty
            if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
            } else {
                sTitle = this.getResourceBundle().getText("worklistTableTitle");
            }
            this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
        },

        /**
         * Event handler when a table item gets pressed
         * @param {sap.ui.base.Event} oEvent the table selectionChange event
         * @public
         */
        onPress : function (oEvent) {
            // The source is the list item that got pressed
            this._showObject(oEvent.getSource());
        },        
        
        onCreate: function () {
            var dialog = this.byId("dialog");

            dialog.open();
        },

        onEmpCreate: function () {
            var dialog = this.byId("_IDGenDialog1");

            dialog.open();
        },

        onClientCreate: function () {
            var dialog = this.byId("_IDGenDialog2");

            dialog.open();
        },

        onDialogCancelButton: function (oEvent) {
            var dialog = this.byId("dialog")
            dialog.close()
        },

        onDialogClientCancelButton: function (oEvent) {
            var dialog = this.byId("_IDGenDialog2")
            dialog.close()
        },

        onDialogEmpCancelButton: function (oEvent) {
            var dialog = this.byId("_IDGenDialog1")
            dialog.close()
        },

        AddProduct: function(oEvent){

            var proname = this.getView().byId("Proname").getValue()
            var prowei = this.getView().byId("Prowei").getValue()
            var shodes = this.getView().byId("Shodes").getValue()
            var Supnam = this.getView().byId("Supnam").getValue()
            var Picurl = this.getView().byId("Picurl").getValue()
            var Price = this.getView().byId("Price").getValue()
            var dimenWi = this.getView().byId("DimensionWidth").getValue()
            var dimenDe = this.getView().byId("DimensionDepth").getValue()
            var dimenHei = this.getView().byId("DimensionHeight").getValue()
            var quan = this.getView().byId("Quantity").getValue()
            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Productid": "",
                "Category":"CD",
                "Maincategory":"CD",
                "Suppliername": Supnam,
                "Weight": prowei ,
                "Weightunit": "KG",
                "Shortdescription": shodes ,
                "Name": proname,
                "Pictureurl" : Picurl ,
                "Status": "100",
                "Price" : Price,
                "Dimensionwidth": dimenWi,
                "Dimensiondepth": dimenDe,
                "Dimensionheight": dimenHei,
                "Unit": "cm",
                "Currencycode": "MAD",
                "Quantity": quan,
                "Unitquantity": "Pce"
            }

            this.addProductToDB(OEntry, oModel)

        },

        addProductToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZPRODUCTSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("dialog")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },

        /**
         * Event handler for navigating back.
         * Navigate back in the browser history
         * @public
         */
        onNavBack : function() {
            // eslint-disable-next-line sap-no-history-manipulation
            history.go(-1);
        },

        handleUploadComplete: function(oEvent) {
			var sResponse = oEvent.getParameter("response"),
				iHttpStatusCode = parseInt(/\d{3}/.exec(sResponse)[0]),
				sMessage;

			if (sResponse) {
				sMessage = iHttpStatusCode === 200 ? sResponse + " (Upload Success)" : sResponse + " (Upload Error)";
				MessageToast.show(sMessage);
			}
		},


        onSearch : function (oEvent) {
            var aFilter = [];
            var sQuery = oEvent.getSource().getValue();
    
            if (sQuery && sQuery.length > 0) {
              aFilter.push(
                new Filter("Name", FilterOperator.Contains, sQuery)
              );
            }
    
            var oTable = this.byId("productTable");
            var oBinding = oTable.getBinding("items");
            oBinding.filter(aFilter);
        },

        /**
         * Event handler for refresh event. Keeps filter, sort
         * and group settings and refreshes the list binding.
         * @public
         */
        onRefresh : function () {
            var oTable = this.byId("table");
            oTable.getBinding("items").refresh();
        },

        /* =========================================================== */
        /* internal methods                                            */
        /* =========================================================== */

        /**
         * Shows the selected item on the object page
         * @param {sap.m.ObjectListItem} oItem selected Item
         * @private
         */
        _showObject : function (oItem) {
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext().getPath().substring("/ZPRODUCTSet".length)
            });
        },

        onDefaultDialogPress :  function () {
            if(!this.oDefaultDialog) {
                this.oDefaultDialog = new Dialog({
                    title: "Create product",
                    content: new List({
						items: {
							path: "/ZPRODUCTSet",
							template: new StandardListItem({
								title: "{Name}",
                                icon:"{Pictureurl}",
                                counter: 1
							})
						}
					}),
                    endButton: new Button({
						text: "Close",
						press: function () {
							this.oDefaultDialog.close();
						}.bind(this)
					})
                });
                this.getView().addDependent(this.oDefaultDialog);
            }
            this.oDefaultDialog.open();
        },

        /**
         * Internal helper method to apply both filter and search state together on the list binding
         * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
         * @private
         */
        _applySearch: function(aTableSearchState) {
            var oTable = this.byId("table"),
                oViewModel = this.getModel("worklistView");
            oTable.getBinding("items").filter(aTableSearchState, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aTableSearchState.length !== 0) {
                oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
            }
        },

        onToggleCart: function() {
            this.getRouter().navTo("cart",{}, true);
        },

        addEmp: function(oEvent){


            var firstname = this.getView().byId("_IDGenInput1").getValue()
            var lastname = this.getView().byId("_IDGenInput2").getValue()
            var title = this.getView().byId("_IDGenInput3").getValue()
            var homephone = this.getView().byId("_IDGenInput4").getValue()
            var Picurl = this.getView().byId("_IDGenInput5").getValue()
            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Employeeid": "625061.00",
                "Firstname": firstname.toString(),
                "Lastname": lastname.toString(),
                "Title": title.toString(),
                "Homephone": homephone.toString(),
                "Photo": Picurl.toString()
                
            }

            this.addEmpToDB(OEntry, oModel)

        },

        addEmpToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZEMPLOYEE_SDSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("_IDGenDialog1")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },


        addClient: function(oEvent){


            var companyName = this.getView().byId("_IDGenInput6").getValue()
            var country = this.getView().byId("_IDGenInput7").getValue()
            var city = this.getView().byId("_IDGenInput8").getValue()
            var postalCode = this.getView().byId("_IDGenInput9").getValue()
            var address = this.getView().byId("_IDGenInput10").getValue()
            var phoneNumber = this.getView().byId("_IDGenInput11").getValue()

            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Customerid": "400003",
                "Companyname": companyName.toString(),
                "Country": country.toString(),
                "City": city.toString(),
                "Postalcode": postalCode.toString(),
                "Address": address.toString(),
                "Phonenumber": phoneNumber.toString()
            }

            this.addClientToDB(OEntry, oModel)

        },

        addClientToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZCLIENT_SDSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("_IDGenDialog2")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },

    });
});
