sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
	"sap/ui/util/Storage",
    "sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, MessageToast,Fragment,Storage, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("pfeproduct.controller.App", {

        onInit : function () {
            // apply content density mode to root view
            this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());

        },

        onCheckout: function() {
            var oCart = this.getModel("cartProducts");
            var oModel = this.getModel();
            var oClient = this.getModel("clients");
            var oCartList = Object.values(oCart.getData()["cartEntries"])
            var oOrderHead = {
                "OrderId" : "",
                "Customerid" : this.getView().byId("productInput").getValue().toString(),
                "Customername" : oClient.getProperty("/SelectedClient"),
                "Employeeid" : this.getView().byId("EmployeeInput").getValue().toString(),
                "Orderdate" : "\/Date("+Date.now()+")\/",
                "Requireddate" : "\/Date("+Date.now()+")\/",
                "Shippeddate" : "\/Date("+Date.now()+")\/",
                "Status" : "NOT VALID",
                "Freight" : "0",
                "Shipname" : this.getView().byId("name").getValue().toString(),
                "Shipaddress" : this.getView().byId("_IDGenInput1").getValue().toString() + " " + this.getView().byId("_IDGenInput2").getValue().toString(),
                "Shipcity" : this.getView().byId("_IDGenInput4").getValue().toString(),
                "Shipregion" : this.getView().byId("_IDGenInput6").getValue().toString(),
                "Shippostalcode" : this.getView().byId("_IDGenInput3").getValue().toString(),
                "Shipcountry" : this.getView().byId("country").getSelectedKey()
            } 

               this.createHeader(oModel, oOrderHead, oCartList);
        },

        createHeader : function(oModelf, oOrderHeadf, oCartList) {
            const that = this;
            oModelf.create("/ZORDER_HEADERSSet", oOrderHeadf, {
                success: function (response) {
                    that.createPosts(oModelf, response.OrderId, oCartList, oCartList[0]);
                    var oCartModel = that.getView("pfeproduct.view.App").getModel("cartLength");
                    var length = "0";
                    var totalPrice = "0";
                    oCartModel.setProperty("/cartLength", length);
                    oCartModel.setProperty("/totalPrice", totalPrice);
                    console.log(response.OrderId)
                    oCartModel.setProperty("/OrderId", response.OrderId);
                    that.getRouter().navTo("orderCompleted", {});
                    
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },

        onValueHelpRequest: function (oEvent) {
            var oClientModel = this.getView("pfeproduct.view.App").getModel("clients");
            oClientModel.loadData("/sap/opu/odata/sap/ZODATA_INWI_SD_SRV/ZCLIENT_SDSet")
            this.getView("pfeproduct.view.App").setModel(oClientModel, "clients");
            console.log(oClientModel);
 
			var sInputValue = oEvent.getSource().getValue(),
				oView = this.getView();

			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "pfeproduct.view.ValueHelpDialog",
					controller: this
				}).then(function (oDialog) {
					oView.addDependent(oDialog);
					return oDialog;
				});
			}

			this._pValueHelpDialog.then(function(oDialog) {
                //console.log(oDialog.getBinding("items").filter([new Filter("Companyname", FilterOperator.Contains, sInputValue)]))
				// Create a filter for the binding
				//oDialog.getBinding("items").filter([new Filter("Companyname", FilterOperator.Contains, sInputValue)]);
				// Open ValueHelpDialog filtered by the input's value
				oDialog.open(sInputValue);
			});
		},

        onValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Companyname", FilterOperator.Contains, sValue);

			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

        onValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			oEvent.getSource().getBinding("items").filter([]);

			if (!oSelectedItem) {
				return;
			}
            var oClient = this.getModel("clients");
            oClient.setProperty('/SelectedClient', oSelectedItem.getDescription());
            console.log(oClient.getProperty("/SelectedClient"));
			this.byId("productInput").setValue(oSelectedItem.getTitle());
		},

        onValueHelpRequestEmp: function (oEvent) {
            var oEmployeeModel = this.getView("pfeproduct.view.App").getModel("employees");
            oEmployeeModel.loadData("/sap/opu/odata/sap/ZODATA_INWI_SD_SRV/ZEMPLOYEE_SDSet")
            this.getView("pfeproduct.view.App").setModel(oEmployeeModel, "employees");
 
			var sInputValue = oEvent.getSource().getValue(),
				oView = this.getView();

			if (!this._pValueHelpDialogEmp) {
				this._pValueHelpDialogEmp = Fragment.load({
					id: oView.getId(),
					name: "pfeproduct.view.ValueHelpDialogEmp",
					controller: this
				}).then(function (oDialog) {
					oView.addDependent(oDialog);
					return oDialog;
				});
			}

			this._pValueHelpDialogEmp.then(function(oDialog) {
                //console.log(oDialog.getBinding("items").filter([new Filter("Companyname", FilterOperator.Contains, sInputValue)]))
				// Create a filter for the binding
				//oDialog.getBinding("items").filter([new Filter("Companyname", FilterOperator.Contains, sInputValue)]);
				// Open ValueHelpDialog filtered by the input's value
				oDialog.open(sInputValue);
			});
		},

        onValueHelpSearchEmp: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Firstname", FilterOperator.Contains, sValue);

			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

        onValueHelpCloseEmp: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			oEvent.getSource().getBinding("items").filter([]);

			if (!oSelectedItem) {
				return;
			}
            var oClient = this.getModel("employees");
            oClient.setProperty('/SelectedEmployee', oSelectedItem.getDescription());
            console.log(oClient.getProperty("/SelectedEmployee"));
			this.byId("EmployeeInput").setValue(oSelectedItem.getTitle());
		},

        createPosts : function(oModelf, orderId, oCartList, firstPost){
            const that = this;
            var oOrderPost = {
                "Postid" : "",
                "Orderid" : orderId,
                "Productid" : firstPost.Productid,
                "Unitprice" : (firstPost.Quantity * firstPost.Price).toString(),
                "Quantity" : firstPost.Quantity.toString(),
                "Discount" : "0",
                "Unit" : "MAD",
                "Cuky" : "Pce"
            }

            oModelf.create("/ZORDER_POSTSSet", oOrderPost, {
                success: function(response) {
                    console.log(oCartList[0])
                    oCartList = oCartList.slice(1)
                    if(oCartList.length > 0){
                        that.createPosts(oModelf, orderId, oCartList, oCartList[0]);
                    } else {
                        var cartModel = that.getView("pfeproduct.view.App").getModel("cartProducts");
                        cartModel.setProperty("/cartEntries", Object.assign({}, {}));
                        cartModel.refresh(true);
                        MessageToast.show("Your order passed successfully")
                    }
                },
                error: function(oerr){
                    console.log(oerr);
                }

            })
        }

    });

});