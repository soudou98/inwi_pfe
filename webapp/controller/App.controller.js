sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
	"sap/ui/util/Storage",
], function (BaseController, JSONModel,Storage) {
    "use strict";

    return BaseController.extend("pfeproduct.controller.App", {

        onInit : function () {
            // apply content density mode to root view
            this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
            this.createContent();
        },

        createContent : function() {

            var oData = {
                cartEntries: {}
            }

            var oCartModel = new JSONModel(oData);
            this.getView("pfeproduct.view.App").setModel(oCartModel, "cartProducts");    
            
            var oCartLength = {
                cartLength: "",
                totalPrice: ""
            }

            var oCartLengthModel = new JSONModel(oCartLength);
            this.getView("pfeproduct.view.App").setModel(oCartLengthModel, "cartLength");

            var oClients = {}

            var oClientModel = new JSONModel(oClients);
            console.log(oClientModel.loadData("/sap/opu/odata/sap/ZODATA_INWI_SD_SRV/ZCLIENT_SDSet"))
            this.getView("pfeproduct.view.App").setModel(oClientModel, "clients");

            var oEmployees = {}

            var oEmployeeModel = new JSONModel(oEmployees);
            console.log(oEmployeeModel.loadData("/sap/opu/odata/sap/ZODATA_INWI_SD_SRV/ZEMPLOYEE_SDSet"))
            this.getView("pfeproduct.view.App").setModel(oEmployeeModel, "employees");
        }
    });

});