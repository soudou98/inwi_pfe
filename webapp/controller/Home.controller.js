sap.ui.define([
    "./BaseController"
], function (BaseController) {
    "use strict";

    return BaseController.extend("pfeproduct.controller.Home", {

        /**
         * Navigates to the worklist when the link is pressed
         * @public
         */
        placeOrder : function () {
            this.getRouter().navTo("worklist");
        },

        createEmployee : function () {
            this.getRouter().navTo("worklist");
        },

        displayOrders : function () {
            this.getRouter().navTo("order");
        },

        displayCart : function () {
            this.getRouter().navTo("cart");
        },
        onCreate: function () {
            var dialog = this.byId("dialog");

            dialog.open();
        },

        onEmpCreate: function () {
            var dialog = this.byId("_IDGenDialog1");

            dialog.open();
        },

        onClientCreate: function () {
            var dialog = this.byId("_IDGenDialog2");

            dialog.open();
        },

        onDialogCancelButton: function (oEvent) {
            var dialog = this.byId("dialog")
            dialog.close()
        },

        onDialogClientCancelButton: function (oEvent) {
            var dialog = this.byId("_IDGenDialog2")
            dialog.close()
        },

        onDialogEmpCancelButton: function (oEvent) {
            var dialog = this.byId("_IDGenDialog1")
            dialog.close()
        },

        AddProduct: function(oEvent){

            var proname = this.getView().byId("Proname").getValue()
            var prowei = this.getView().byId("Prowei").getValue()
            var shodes = this.getView().byId("Shodes").getValue()
            var Supnam = this.getView().byId("Supnam").getValue()
            var Picurl = this.getView().byId("Picurl").getValue()
            var Price = this.getView().byId("Price").getValue()
            var dimenWi = this.getView().byId("DimensionWidth").getValue()
            var dimenDe = this.getView().byId("DimensionDepth").getValue()
            var dimenHei = this.getView().byId("DimensionHeight").getValue()
            var quan = this.getView().byId("Quantity").getValue()
            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Productid": "",
                "Category":"CD",
                "Maincategory":"CD",
                "Suppliername": Supnam,
                "Weight": prowei ,
                "Weightunit": "KG",
                "Shortdescription": shodes ,
                "Name": proname,
                "Pictureurl" : Picurl ,
                "Status": "100",
                "Price" : Price,
                "Dimensionwidth": dimenWi,
                "Dimensiondepth": dimenDe,
                "Dimensionheight": dimenHei,
                "Unit": "cm",
                "Currencycode": "MAD",
                "Quantity": quan,
                "Unitquantity": "Pce"
            }

            this.addProductToDB(OEntry, oModel)

        },

        addProductToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZPRODUCTSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("dialog")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },

        addEmp: function(oEvent){


            var firstname = this.getView().byId("_IDGenInput1").getValue()
            var lastname = this.getView().byId("_IDGenInput2").getValue()
            var title = this.getView().byId("_IDGenInput3").getValue()
            var homephone = this.getView().byId("_IDGenInput4").getValue()
            var Picurl = this.getView().byId("_IDGenInput5").getValue()
            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Employeeid": "625061.00",
                "Firstname": firstname.toString(),
                "Lastname": lastname.toString(),
                "Title": title.toString(),
                "Homephone": homephone.toString(),
                "Photo": Picurl.toString()
                
            }

            this.addEmpToDB(OEntry, oModel)

        },

        addEmpToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZEMPLOYEE_SDSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("_IDGenDialog1")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        },

        addClient: function(oEvent){


            var companyName = this.getView().byId("_IDGenInput6").getValue()
            var country = this.getView().byId("_IDGenInput7").getValue()
            var city = this.getView().byId("_IDGenInput8").getValue()
            var postalCode = this.getView().byId("_IDGenInput9").getValue()
            var address = this.getView().byId("_IDGenInput10").getValue()
            var phoneNumber = this.getView().byId("_IDGenInput11").getValue()

            var oModel = this.getOwnerComponent().getModel();


            var OEntry = {
                "Customerid": "400003",
                "Companyname": companyName.toString(),
                "Country": country.toString(),
                "City": city.toString(),
                "Postalcode": postalCode.toString(),
                "Address": address.toString(),
                "Phonenumber": phoneNumber.toString()
            }

            this.addClientToDB(OEntry, oModel)

        },

        addClientToDB : function(OEntry,oModel){
            const that = this;
            oModel.create("/ZCLIENT_SDSet", OEntry, {
                success: function (response) {
                    var dialog = that.byId("_IDGenDialog2")
                    dialog.close()
                },
                error: function (oerr) {
                    console.log(oerr);
                }
            })
        }

    });

});