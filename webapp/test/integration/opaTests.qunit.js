/* global QUnit */

sap.ui.require([
	"pfeproduct/test/integration/AllJourneys"
], function() {
	QUnit.config.autostart = false;
	QUnit.start();
});